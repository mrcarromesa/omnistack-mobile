import React, {useEffect, useState} from 'react';
import { withNavigation } from 'react-navigation';
import {View, Text, Image, FlatList, StyleSheet, TouchableOpacity} from 'react-native';

import api from '../services/api';

//a propriedade navigation so esta disponivies para as paginas conforme arquivo routes
//dessa forma precisa importar o withNavigation de react-navigation
//e adicionar no export default o withNavigation(Funcao)
//e add nas propos da funcao a prop navigation
function SpotList({tech, navigation}) {

    const [spots, setSpots] = useState([]);

    useEffect(
        () => {
            async function loadSpots() {

                
                const response = await api.get('/spots',{
                    params: {tech} 
                });

                console.log(response.data);

                setSpots(response.data);
                

                
            }

            loadSpots();

        }, []
    );

    function handleNavigation(id) {
        navigation.navigate('Book',{id});
    }

    return (
        <View sytle={styles.container}>
            <Text style={styles.title}>Empresas que usam <Text style={styles.bold}>{tech}</Text></Text>
            {/* FlatList para criar lista com scroll */}
            {/* keyExtractor {o item deconstruido => o item.propidade} seria tipo o map obtendo o object da propriedade data={} */}
            {/* o renderItem obtem o item que nesse caso seria do object spots ja como estives dentro de um for e extrair as propriedades dele */}
            {/* Quando a imagem pega de uma uri, tem que utilizar no source a chamada uri para identificar isso */}
            <FlatList 
                style={styles.list}
                data={spots}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={spot => spot._id}
                renderItem={({item}) => (
                    <View style={styles.listItem} >
                        <Image style={styles.thumbnail} source={{ uri: item.thumbnail_url}} />
                        <Text style={styles.company}>{item.company}</Text>
                        <Text style={styles.price}>{item.price ? `R$${item.price}` : 'GRATUITO' }</Text>
                        <TouchableOpacity onPress={() => handleNavigation(item._id)} style={styles.button}>
                            <Text style={styles.buttonText}>Solicitar Reserva</Text>
                        </TouchableOpacity>
                    </View>
                )}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop:30,
        paddingTop: 30,
    },

    title: {
        fontSize: 20,
        color: '#444',
        paddingHorizontal: 20,
        marginBottom: 15
    },

    bold: {
        fontWeight: "bold"
    },

    list : {
        paddingHorizontal: 20,
    },

    listItem: {
        marginRight: 15
    },

    thumbnail: {
        width: 200,
        height: 120,
        resizeMode: 'cover',
        borderRadius: 2,
    },

    company: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#333',
        marginTop: 10
    },

    price: {
        fontSize: 15,
        color: '#999',
        marginTop: 5,
    },

    button: {
        height: 32,
        backgroundColor: '#f05a5b',
        justifyContent: 'center',
        alignItems: 'center', //alinha o texto no total centro do botao
        borderRadius: 2,
        marginTop: 15
    },

    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 15, 
    }

});

export default withNavigation(SpotList);