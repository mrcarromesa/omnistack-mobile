import React, {useState, useEffect} from 'react';
import {View, Image, Text, TextInput, TouchableOpacity, StyleSheet, KeyboardAvoidingView, Platform, AsyncStorage } from 'react-native';

import api from '../services/api';

import logo from '../assets/logo.png';

export default function Login({ navigation }) {

    const [email, setEmail] = useState('');
    const [techs, setTechs] = useState('');

    //recebe 2 propriedades, 2 parametros
    //1 - o que quer executar
    //2 - array de dependencias que e quando queremos executar essa primeira funcao
    //se deixar o array vazio ele vai executar apenas uma vez 
    useEffect(() => {
        //Previnir a atualizacao de tela command + R
        AsyncStorage.getItem('user').then((user) => {
            if (user) {
                navigation.navigate('List');
            }
        });
    }, []);


    async function handleSubmit(){
        const response = await api.post('/sessions', {
            email
        });

        const { _id } = response.data;

        await AsyncStorage.setItem('user', _id);
        await AsyncStorage.setItem('techs', techs);

        //console.log(_id);

        navigation.navigate('List');
    }


    return (
            
        <KeyboardAvoidingView behavior="padding" enabled={Platform.OS == 'ios' } style={styles.container}>
            {/* KeyboardAvoidingView evita que o teclado fique acima do input */}  
            <Image source={logo} />
            <View style={styles.form}>
                <Text style={styles.label}>SEU E-MAIL *</Text>
                <TextInput
                    style={styles.input}
                    placeholder="SEU E=MAIL*"
                    placeholderTextColor="#999"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                    value={email}
                    onChangeText={text => setEmail(text)}
                    />
                
                <Text style={styles.label}>TECNOLOGIAS *</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Tecnologias de interesse"
                    placeholderTextColor="#999"
                    autoCapitalize="words"
                    autoCorrect={false}
                    value={techs}
                    onChangeText={setTechs}
                    />
                    <TouchableOpacity onPress={handleSubmit} style={styles.button}>
                        <Text style={styles.buttonText}>Encontrar Spots</Text>
                    </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
        
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1, //para ocupar todo o tamanho da tela
        justifyContent: "center", //alinhar o conteudo centralizado verticalmente
        alignItems: "center", //alinhar centralizado horizontalmente 
    },


    form: {
        alignSelf: "stretch", //stretch = match_parent **aparentemente
        paddingHorizontal: 30,
        marginTop: 30
    },

    label: {
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8,
    },

    input: {
        borderWidth: 1,
        borderColor: "#ddd",
        paddingHorizontal: 20,
        fontSize: 16,
        color: "#444",
        height: 44,
        marginBottom: 20, 
        borderRadius: 2,
    },

    button: {
        height: 42,
        backgroundColor: '#f05a5b',
        justifyContent: 'center',
        alignItems: 'center', //alinha o texto no total centro do botao
        borderRadius: 2,
    },

    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16, 
    }
});